//
//  AssetListViewController.swift
//  radio_ui
//
//  Created by Cody Nelson on 4/3/18.
//  Copyright © 2018 Bonneville. All rights reserved.
//

import UIKit
import radio_core
import AVFoundation
import AVKit

public class AssetListTableViewController: UITableViewController {
    

    
    // MARK: Properties
    
    public static let presentPlayerViewControllerSegueID = "PresentPlayerViewControllerSegueIdentifier"
    
    fileprivate var playerViewController: AVPlayerViewController?
    
    // MARK: Storyboard Properties

    
    // MARK: --

    // MARK: Deinitialization
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: .AssetListManagerDidLoad,
                                                  object: nil)
    }
    
    // MARK: UIViewController

    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // General setup for auto sizing UITableViewCells.
        tableView.estimatedRowHeight = 75.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        
        // Set AssetListTableViewController as the delegate for AssetPlaybackManager to recieve playback information.
        AssetPlaybackManager.sharedManager.delegate = self
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleAssetListManagerDidLoad(_:)),
                                               name: .AssetListManagerDidLoad, object: nil)
        

        // Do any additional setup after loading the view.
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        if playerViewController != nil {
            // The view reappeared as a results of dismissing an AVPlayerViewController.
            // Perform cleanup.
            AssetPlaybackManager.sharedManager.setAssetForPlayback(nil)
            playerViewController?.player = nil
            playerViewController = nil
        }
    }
    

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: Table View Data Source
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AssetListManager.sharedManager.numberOfAssets()
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AssetListTableViewCell.reuseIdentifier, for: indexPath)
        
        let asset = AssetListManager.sharedManager.asset(at: indexPath.row)
        
        if let cell = cell as? AssetListTableViewCell {
            cell.asset = asset
            cell.delegate = self
        }
        
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? AssetListTableViewCell,
            let asset = cell.asset else { return }
        
        let downloadState = AssetPersistenceManager.sharedManager.downloadState(for:asset)
        let alertAction: UIAlertAction
        
        switch downloadState {
        case .notDownloaded:
            alertAction = UIAlertAction(title: "Download", style: .default) { _ in
                AssetPersistenceManager.sharedManager.downloadStream(for: asset)
            }
        case .downloading:
            alertAction = UIAlertAction(title: "Cancel", style: .default) { _ in
                AssetPersistenceManager.sharedManager.cancelDownload(for: asset)
            }
        case .downloaded:
            alertAction = UIAlertAction(title: "Delete", style: .default) { _ in
                AssetPersistenceManager.sharedManager.deleteAsset(asset)
            }
        }
        
        let alertController = UIAlertController(title: asset.stream.name, message: "Select from the following options:", preferredStyle: .actionSheet)
        alertController.addAction(alertAction)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: nil))
        
        if UIDevice.current.userInterfaceIdiom == .pad{
            guard let popoverController = alertController.popoverPresentationController else {
                return
            }
            popoverController.sourceView = cell
            popoverController.sourceRect = cell.bounds
            
        }
        
        present(alertController, animated: true, completion:  nil)
        
    }
    
    override public func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if segue.identifier == AssetListTableViewController.presentPlayerViewControllerSegueID {
            guard let cell = sender as? AssetListTableViewCell,
                let playerViewController = segue.destination as? AVPlayerViewController else { return }
            
            /*
             Grab a reference for the destinationViewController to use in later delegate callbacks from
             AssetPlaybackManager.
             */
            playerViewController = playerViewController
           
            
            // Load the new Asset to playback into AssetPlaybackManager.
            AssetPlaybackManager.sharedManager.setAssetForPlayback(cell.asset)
        }

    }
    
    // MARK: Notification handling
    
    @objc public func handleAssetListManagerDidLoad(_: Notification) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
}

/**
Extend `AssetListTableViewController` to conform to the `AssetListTableViewCellDelegate` protocol.
*/
extension AssetListTableViewController: AssetListTableViewCellDelegate {
    
    func assetListTableViewCell(_ cell: AssetListTableViewCell, downloadStateDidChange newState: Asset.DownloadState) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
}

/**
 Extend `AssetListTableViewController` to conform to the `AssetPlaybackDelegate` protocol.
 */
extension AssetListTableViewController: AssetPlaybackDelegate {
    public func streamPlaybackManager(_ streamPlaybackManager: AssetPlaybackManager, playerReadyToPlay player: AVPlayer) {
        player.play()
    }
    
    public func streamPlaybackManager(_ streamPlaybackManager: AssetPlaybackManager,
                               playerCurrentItemDidChange player: AVPlayer) {
        guard let playerViewController = playerViewController,
            player.currentItem != nil else { return }
        
        playerViewController.player = player
    }
}

